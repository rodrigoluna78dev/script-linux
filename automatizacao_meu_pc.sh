#!/bin/bash

## Adicionar repositório Flathub ##

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## INSTLANDO A LISTA DE PACOTES APÓS ADIÇÃO DO REPOSITÓRIO ##

echo "INSTLANDO A LISTA DE PACOTES APÓS ADIÇÃO DO REPOSITÓRIO"

flatpak install com.transmissionbt.Transmission -y &&
flatpak install org.libreoffice.LibreOffice -y &&
flatpak install com.gitlab.davem.ClamTk -y &&
flatpak install net.codeindustry.MasterPDFEditor -y &&
flatpak install com.jgraph.drawio.desktop -y &&
flatpak install org.apache.netbeans -y &&
flatpak install com.skype.Client -y &&
flatpak install us.zoom.Zoom -y &&
flatpak install org.fedoraproject.MediaWriter -y &&
flatpak install io.dbeaver.DBeaverCommunity -y &&

## ATUALIZAR FLATPAKS E ARQUIVOS FALTANTES ##

echo "ATUALIZANDO OS FLATPAKS"

flatpak -y update

## ADICIONANDO VSCODE AO REPOSITORIO DO SISTEMA ##

echo "INICIANDO... " 

sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

echo "ADICIONADO " 

## FIM DOS REPOSITORIOS EXTERNOS ##

## INSTALANDO nodejs git evolution deja-dup ##

sudo dnf -y install vscode nodejs git evolution deja-dup

## FIM DA INSTALACAO DOS RPMS

## INSTALAR REPOSITÓRIOS DO RPMFUSION PARA NVIDIA""

echo "INSTALAR REPOSITÓRIOS DO RPMFUSION PARA NVIDIA"

sudo rpm-ostree install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 
sudo rpm-ostree install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

## INSTALANDO O DOCKER ##

echo "INSTALANDO O DOCKER"

sudo wget -c https://download.docker.com/linux/fedora/docker-ce.repo
sudo cp -f docker-ce.repo /etc/yum.repos.d/
rpm-ostree install docker-ce docker-ce-cli containerd.io docker-compose-plugin

## INSTALANDO O BANCO DE DADOS ##

echo "INSTALANDO O BANCO DE DADOS"

wget -c https://download.postgresql.org/pub/repos/yum/reporpms/F-36-x86_64/pgdg-fedora-repo-latest.noarch.rpm
sudo rpm-ostree install pgdg-fedora-repo-latest.noarch.rpm
rpm-ostree install postgresql postgresql-server postgresql-devel postgresql-contrib postgresql-docs
postgresql-setup --initdb --unit postgresql
sudo systemctl enable postgresql.service
sudo systemctl start postgresql.service

## INSTALANDO O KVM PARA O ANDROID ##

echo "INSTALANDO O KVM PARA ANDROID"

rpm-ostree install bridge-utils libvirt virt-install qemu-kvm libvirt-devel virt-top libguestfs-tools virt-manager

## FIM DA INSTALACAO DO KVM ##
echo "FIM DA INSTALACAO DO KVM"
echo "FIM DA INSTALACAO!"
## REINICIANDO O SISTEMA ##
echo "REINICIANDO O SISTEMA"
sudo systemctl reboot