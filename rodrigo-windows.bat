## Execute a primeira no PowerShell como ADM e rode o script normalmente. ##
## Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine ##
Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://ohmyposh.dev/install.ps1'))
oh-my-posh init pwsh | Invoke-Expression
@echo off 
Echo Installing programs... 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user opticos.gwsl 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user gnome.Dia
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user darktable.darktable 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user RARLab.WinRAR 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user Postman.Postman
winget install --accept-package-agreements --accept-source-agreements -h --scope user --id PostgreSQL.PostgreSQL 15
winget install --accept-package-agreements --accept-source-agreements -h --scope user --id PostgreSQL.pgAdmin 4
winget install --accept-package-agreements --accept-source-agreements -h --scope user dbeaver.dbeaver
winget install --accept-package-agreements --accept-source-agreements -h --scope user Piriform.Recuva 
winget install --accept-package-agreements --accept-source-agreements -h --scope user Oracle.MySQL 
winget install --accept-package-agreements --accept-source-agreements -h --scope user OpenJS.NodeJS.LTS 
winget install --accept-package-agreements --accept-source-agreements -h --scope user OBSProject.OBSStudio 
winget install --accept-package-agreements --accept-source-agreements -h --scope user Microsoft.WindowsAdminCenter
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user Microsoft.WindowsTerminal 
winget install --accept-package-agreements --accept-source-agreements -h --scope user Microsoft.VisualStudio.2022.Community 
winget install --accept-package-agreements --accept-source-agreements -h --scope user KDE.umbrello
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user JetBrains.IntelliJIDEA.Community 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user JGraph.Draw
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user Google.AndroidStudio 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user GitHub.Atom
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user Figma.Figma 
winget install --accept-package-agreements --accept-source-agreements -h --scope user Dropbox.Dropbox 
winget install --accept-package-agreements --accept-source-agreements -h --scope user Docker.DockerDesktop 
winget install --accept-package-agreements --accept-source-agreements --force -h --scope user Discord.Discord
if %ERRORLEVEL% EQU 0 Echo All installed!