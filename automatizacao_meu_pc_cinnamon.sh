#!/bin/bash

## Adicionar repositório Flathub ##

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## INSTLANDO A LISTA DE PACOTES APÓS ADIÇÃO DO REPOSITÓRIO ##

echo "INSTLANDO A LISTA DE PACOTES APÓS ADIÇÃO DO REPOSITÓRIO"


flatpak install com.gitlab.davem.ClamTk -y &&
flatpak install net.codeindustry.MasterPDFEditor -y &&
flatpak install org.apache.netbeans -y &&
flatpak install com.microsoft.Edge -y &&
flatpak install com.skype.Client -y &&
flatpak install us.zoom.Zoom -y &&
flatpak install org.codeblocks.codeblocks -y &&
flatpak install io.dbeaver.DBeaverCommunity -y &&

## ATUALIZAR FLATPAKS E ARQUIVOS FALTANTES ##

echo "ATUALIZANDO OS FLATPAKS"

flatpak -y update

## INSTALANDO VSCODE NODEJS EVOLUTION E DEJA-DUP##

echo "INICIANDO... " 

sudo dnf -y install vscode nodejs git evolution deja-dup

## FIM DA INSTALACAO DO VSCODE NODEJS EVOLUTION GIT ##

echo "FIM DA INSTALACAO DO VSCODE NODEJS EVOLUTION GIT"

echo "FIM DA INSTALACAO!"
## REINICIANDO O SISTEMA ##
echo "REINICIANDO O SISTEMA"
sudo systemctl reboot